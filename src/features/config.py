# Main page block
main_page_block = {
    'Переводчик': '//a[text()="Переводчик"]'
}

# Translate page block
translate_page_block = {
    'Поле ввода': '//div[@id="fakeArea"]',
    'Войти': '//a[@class="button button_enter"]',
}

# Login page block
login_page_block = {
    'Ошибка': {
        'Такого аккаунта нет': '//div[text()="Такого аккаунта нет"]',
        'Пароль не указан': '//div[contains(text(), "Пароль не")]',
        'Логин не указан': '//div[contains(text(), "Логин не")]',
    },
    'Войти': '//button[@type="submit"]',
    'Логин': '//input[@name="login"]',
    'Пароль': '//input[@name="passwd"]',
}
