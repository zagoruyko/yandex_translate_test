from behave import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait as Wait
from config import *
import time


BLOCK = None
TIMEOUT = 10


@when(u'Начали заполнять блок "{block_name}"')
def step(context, block_name):
    global BLOCK
    BLOCK = block_name.lower()


@given(u'Открыть сайт Яндекс')
def step(context):
    context.browser.get('https://yandex.ru')


@when(u'Нажали на ссылку "{link}"')
def step(context, link):
    if BLOCK == "главная страница":
        link_el = main_page_block[link]
        translate_btn = context.browser.find_element_by_xpath(link_el)
        translate_btn.click()
    elif BLOCK == "страница переводчика":
        btn = translate_page_block[link]
        btn_elem = context.browser.find_element_by_xpath(btn)
        btn_elem.click()
    elif BLOCK == 'страница авторизации':
        login_page = login_page_block[link]
        btn = context.browser.find_element_by_xpath(login_page)
        btn.click()
    else:
        raise Exception('Error has occured', BLOCK)


@when(u'Нажали на кнопку "{link}"')
def step(context, link):
    if BLOCK == "главная страница":
        link_el = main_page_block[link]
        translate_btn = context.browser.find_element_by_xpath(link_el)
        translate_btn.click()
    elif BLOCK == "страница переводчика":
        btn = translate_page_block[link]
        btn_elem = context.browser.find_element_by_xpath(btn)
        btn_elem.click()
    elif BLOCK == 'страница авторизации':
        login_page = login_page_block[link]
        btn = context.browser.find_element_by_xpath(login_page)
        btn.click()
    else:
        raise Exception('Error has occured', BLOCK)


@when('В поле "{field_name}" ввели "{field_value}"')
def step(context, field_name, field_value):
    if BLOCK == 'страница авторизации':
        field = context.browser.find_element_by_xpath(login_page_block[field_name])
        field.send_keys(field_value)

    elif BLOCK == "страница переводчика":
        text_area = translate_page_block[field_name]
        elem = context.browser.find_element_by_xpath(text_area)
        elem.send_keys((field_value, Keys.ENTER))


@When('Убедится, что ошибка "{error_text}" присутствует на странице')
def step(context, error_text):
    if BLOCK == 'страница авторизации':
        error_xpath = login_page_block['Ошибка']
        visibility_of_elem = Wait(context.browser, TIMEOUT).until(
            EC.text_to_be_present_in_element((By.XPATH, error_xpath[error_text]), error_text))
        assert visibility_of_elem
        context.browser.refresh()

